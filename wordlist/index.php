<?php

$chars = str_split('abcdefghijklmnopqrstuvwxyz', 1);

$content = '';
for ($i = 0; $i < count($chars); $i++) {
    $a = $chars[$i];
    $upper_a = strtoupper($a);
    for ($j = 0; $j < count($chars); $j++) {
        $b = $chars[$j];
        $upper_b = strtoupper($b);
        $content .= "$a$b $a$upper_b $upper_a$b $upper_a$upper_b\n";
    }
}

file_put_contents('index.txt', $content);
