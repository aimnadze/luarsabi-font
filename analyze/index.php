#!/usr/bin/php
<?php

if (count($argv) < 2) {
    include_once 'fns/usage.php';
    usage();
}

$files = [];
for ($i = 1; $i < count($argv); $i++) {
    include_once 'fns/process.php';
    $filename = $argv[$i];
    $files[$filename] = process($filename);
}

include_once 'fns/compare_files.php';
compare_files($files);
