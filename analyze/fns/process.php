<?php

function process ($filename) {

    echo 'INFO: Reading ' . json_encode($filename) . "\n";

    if (!is_file($filename)) die("ERROR: File not found\n");

    $content = file_get_contents($filename);
    preg_match_all('/StartChar[\s\S]*?EndChar/m', $content, $matches);

    $chars = [];
    foreach ($matches[0] as $i => $match) {

        echo "INFO: $i: Parsing\n";

        if (!preg_match('/Encoding: \d+ (\d+) \d+/', $match, $encoding_match)) {
            die("ERROR: $i: Error parsing \"Encoding\"\n");
        }

        $char = html_entity_decode('&#x' . dechex($encoding_match[1]) . ';', ENT_QUOTES);
        echo "INFO: $i: Found char $char\n";

        if (array_key_exists($char, $chars)) {
            die("ERROR: $i: Duplicate char found\n");
        }

        $char_data = [
            'char' => $char,
            'width' => 0,
            'spline_set' => '',
            'kerns2' => [],
        ];

        if (preg_match('/Width: (\d+)/', $match, $width_match)) {
            $char_data['width'] = (int)$width_match[1];
        }

        if (!in_array($char_data['width'], [360, 380, 580, 600, 700, 780, 830, 880, 900, 980, 1000, 1020, 1040, 1060, 1080, 1180, 1220])) {
            var_dump($char);
            die("ERROR: Wierd width $char_data[width]\n");
        }

        if (preg_match('/SplineSet([\s\S]*?)EndSplineSet/', $match, $spline_set_match)) {
            $char_data['spline_set'] = $spline_set_match[1];
        }

        if (preg_match('/Kerns2: (.*)/', $match, $kern_match)) {
            foreach (array_chunk(explode(' ', $kern_match[1]), 3) as $triple) {
                $char_data['kerns2'][$triple[0]] = $triple;
            }
        }

        $chars[$char] = $char_data;

    }

    foreach ($chars as &$char) {
        ksort($char['kerns2']);
        $portable_kerns2 = [];
        foreach ($char['kerns2'] as $index => $triple) {
            $portable_kerns2[array_keys($chars)[$index]] = $triple;
        }
        $char['portable_kerns2'] = $portable_kerns2;
    }
    unset($char);

    include_once __DIR__ . '/check_case.php';
    check_case($chars);

    foreach ($chars as $char => $char_data) {
        include_once __DIR__ . '/check_advance.php';
        check_advance($char, $char_data);
    }

    return $chars;

}
