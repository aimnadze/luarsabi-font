<?php

function check_spline_set ($char, $remote_char, $chars) {

    $spline_set = $chars[$char]['spline_set'];
    $remote_spline_set = $chars[$remote_char]['spline_set'];

    if ($spline_set !== $remote_spline_set) {
        die("ERROR: Spline set of $char and $remote_char differ\n");
    }

}
