<?php

function check_case ($chars) {

    $lowercase = [
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
        'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
        'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
        'y', 'z',
    ];

    $uppercase = [
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
        'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
        'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
        'Y', 'Z',
    ];

    $similars = [
        ['3', 'ვ'],
    ];

    foreach ($lowercase as $i => $char) {

        $upper_char = $uppercase[$i];
        if (!array_key_exists($upper_char, $chars)) {
            die("ERROR: Char $upper_char uppercase of $char not found\n");
        }

        include_once __DIR__ . '/check_kerning.php';
        check_kerning($char, $upper_char, $chars);

        include_once __DIR__ . '/check_spline_set.php';
        check_spline_set($char, $upper_char, $chars);

    }

    foreach ($uppercase as $i => $char) {

        $lower_char = $lowercase[$i];
        if (!array_key_exists($lower_char, $chars)) {
            die("ERROR: Char $lower_char lowercase of $char not found\n");
        }

        include_once __DIR__ . '/check_kerning.php';
        check_kerning($char, $lower_char, $chars);

        include_once __DIR__ . '/check_spline_set.php';
        check_spline_set($char, $lower_char, $chars);

    }

    foreach ($similars as $similar_chars) {
        foreach ($similar_chars as $i => $char) {

            if (!array_key_exists($char, $chars)) {
                die("ERROR: Char $char not found\n");
            }

            if ($i === 0) continue;

            include_once __DIR__ . '/check_spline_set.php';
            check_spline_set($char, $similar_chars[0], $chars);

        }
    }

}
