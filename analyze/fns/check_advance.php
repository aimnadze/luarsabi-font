<?php

function check_advance ($char, $char_data) {
    foreach ($char_data['portable_kerns2'] as $kern_char => $triple) {

        $advance = (int)$triple[1];

        if (!in_array($advance, [-220, -200, -190, -170, -160, -120, -110, -100, -90, -80, -70, -60, -50, -40, -30, -20, -10])) {
            die("ERROR: Wierd advance $advance on $char$kern_char\n");
        }

    }
}
