<?php

function compare_files ($files) {
    foreach ($files as $filename => $chars) {
        foreach ($chars as $char => $char_data) {
            foreach ($files as $other_filename => $other_chars) {

                if (!array_key_exists($char, $other_chars)) {
                    die("ERROR: Char $char found in $filename but not in $other_filename\n");
                }

                $other_char_data = $other_chars[$char];

                $kerns2 = $char_data['portable_kerns2'];
                $other_kerns2 = $other_char_data['portable_kerns2'];

                foreach ($kerns2 as $kern_char => $triple) {

                    if (!array_key_exists($kern_char, $other_kerns2)) {
                        echo "ERROR: Kerning of $char$kern_char found in $filename but not in $other_filename\n";
                        continue;
                    }

                    if ($triple[1] !== $other_kerns2[$kern_char][1]) {
                        echo "ERROR: Advance of $char$kern_char differ in $filename and in $other_filename\n";
                        continue;
                    }

                }

            }
        }
    }
}
